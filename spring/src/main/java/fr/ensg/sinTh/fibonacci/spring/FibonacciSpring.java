package fr.ensg.sinTh.fibonacci.spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import static fr.ensg.sinTh.fibonacci.core.FibonacciCore.fibonacci;

@RestController
@EnableAutoConfiguration
public class FibonacciSpring {

    @GetMapping("/fibonacci/{id}")
    public String fibo(@PathVariable long id) {
        return "Le Fibonacci du "+id+ " est égale à " + fibonacci(id, 0, 1);
    }

    public static void main(String[] args) {
        SpringApplication.run(FibonacciSpring.class, args);
    }

}
