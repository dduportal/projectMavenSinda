package fr.ensg.sinTh.fibonacci.plugin;

import fr.ensg.sinTh.fibonacci.core.FibonacciCore;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

@Mojo(name="fibonacci")
public class FibonacciPlugin extends AbstractMojo {

    @Parameter(property="rank")
    private long rank;

    public  void execute() throws MojoExecutionException {
        System.out.println(FibonacciCore.fibonacci(rank, 0, 1)) ;
    }
}